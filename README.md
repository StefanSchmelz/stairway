# Stairway 
A Project to provide a wifi capable controller for lighting effects on Stairs. 

## Prerequisites:
- A Stairway that is prepared with a led strip per Step. (In the moment the project is only ment for
  single color strips, but will be Expanded to multicolor Strips in Future.) 
- High side Wiring of the led strips, because this board does low-side-switching. 
- Triggers for the effects. The board provides contacts to attach motion sensors or light barriers,
  but also provides urls to trigger effects.
- 2.4GHz Wifi to connect the esp8266 to. 

## Components:
1. Wemos D1 mini - Microcontroller
2. PCA9685 - 16 channel pwm controller
3. PCA9306 - i2c level shifter

